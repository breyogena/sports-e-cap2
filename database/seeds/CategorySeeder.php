<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
        	'name' => 'Basketball',
            // 'category_code'  => strtoupper(Str::random(10))
            'total' => 1,
            'available' => 1

        ]);

         DB::table('categories')->insert([
            'name' => 'Volleyball',
             // 'category_code'  => strtoupper(Str::random(10))
             'total' => 1,
            'available' => 1
        ]);

        DB::table('categories')->insert([
        	'name' => 'Boxing',
            // 'category_code'  => strtoupper(Str::random(10))
             'total' => 1,
            'available' => 1
        ]);

        DB::table('categories')->insert([
        	'name' => 'Badminton',
             // 'category_code'  => strtoupper(Str::random(10))
             'total' => 1,
            'available' => 1
        ]);
    }
}
