<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
    Alert::success('Hello');
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

// Route::delete('/products/create/{id}', 'ProductController@store')->name('products.store');
Route::delete('/cart', 'CartController@clearAll')->name('cart.clearAll');

Route::resources([
	'categories' => 'CategoryController',
	'products' => 'ProductController',
	'cart' => 'CartController',
	'transactions' => 'TransactionController',
	'dashboards' => 'DashboardController'
]);
