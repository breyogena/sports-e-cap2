<div class="card mb-3 " id="card-cat">
  <div class="card-body">
    <h4 class="cart-title text-center text-uppercase">
      {{ $category->name }}
    </h4>
    <div class="text-center">
      <small class="font-weight-bold">Stocks available: 
        {{$count}}/{{$count_total}}
      </small>
    </div>
  </div>
  <div class="card-footer text-center ">
    @if(!isset($view))
      {{-- view --}}
      <a href="{{ route('categories.show', $category->id) }}" class="btn btn-outline-primary btn-sm w-100 mx-2 my-1 mx-auto font-weight-bold text-dark">
        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.134 13.134 0 0 0 1.66 2.043C4.12 11.332 5.88 12.5 8 12.5c2.12 0 3.879-1.168 5.168-2.457A13.134 13.134 0 0 0 14.828 8a13.133 13.133 0 0 0-1.66-2.043C11.879 4.668 10.119 3.5 8 3.5c-2.12 0-3.879 1.168-5.168 2.457A13.133 13.133 0 0 0 1.172 8z"/>
          <path fill-rule="evenodd" d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
        </svg>
        View / Add unit
      </a>
    @endif
    {{-- edit --}}
    {{-- <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-warning btn-sm mx-2">Edit</a> --}}
    <!-- Button trigger modal -->
    @can('isAdmin')
      <button type="button" class="btn btn-outline-primary btn-sm mx-2 w-100 my-1 mx-auto font-weight-bold text-dark" data-toggle="modal" data-target="#staticBackdrop1{{$category->id}}">
        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
          <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
        </svg>
        Edit
      </button>
    {{-- delete --}}
      <button type="button" class="btn btn-outline-danger btn-sm mx-2 w-100 my-1 mx-auto font-weight-bold text-dark" data-toggle="modal" data-target="#staticBackdrop2{{$category->id}}">
        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
          <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
        </svg>
        Delete
      </button>
    @endcan
  </div>
</div>

<!-- Modal Edit-->
<div class="modal fade" id="staticBackdrop1{{$category->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Edit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="col-12">
        <h1 class="text-center">
          Edit Item
        </h1>
      </div>

      
        {{-- start form --}}
        <form action="{{ route('categories.update', $category->id) }}" method="post">
          @csrf
          @method('PUT')
          <label for="name">Category name:</label>
          <input 
            type="text" 
            name="name" 
            id="name" 
            class="form-control 
              form-control-sm 
              @error('name')is-invalid
              @enderror
            " 
            value="{{ $category->name }}" 
            autofocus
            autocomplete="name" 
            required 
          >
          @error('name')
            <small class="d-block invalid-feedback">
              <strong>{{ $message }}</strong>
            </small>
          @enderror
          <button class="btn btn-sm btn-primary mt-1">Edit</button>
        </form>
        {{-- end form --}}
        @include('sweetalert::alert')
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>


<!-- Modal Delete-->
<div class="modal fade" id="staticBackdrop2{{$category->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Delete</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5 class="text-center">Are you sure you want to delete {{$category->name}}?</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
        <form action="{{ route('categories.destroy', $category->id) }}" method="post" class="d-inline-block mx-2">
          @csrf
          @method('DELETE')
          <button class="btn btn-danger btn-sm">Delete</button>
        </form>
      </div>
    </div>
  </div>
</div>