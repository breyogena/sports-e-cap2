<form action="{{ route('products.edit', $product->id) }}" method="post">
	@csrf
	@method("PUT")
	<button type="button" class="btn btn-sm btn-warning w-100 mb-2 font-weight-bold" data-toggle="modal" data-target="#staticBackdrop{{$product->id}}">
		<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
          <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
        </svg>
		Edit
	</button>
</form>

<!-- Modal for edit product-->
<div class="modal fade" id="staticBackdrop{{$product->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Edit Unit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="col-12 col-md-8 col-lg-6 mx-auto">
      		<h1 class="text-center">Edit Unit</h1>
      	</div>

      	
				<form 
					action="{{ route('products.update', $product->id)}}"
					method="post" 
					enctype="multipart/form-data" 
				>
					@csrf
					@method('PUT')

					{{-- name --}}
					@include('products.partials.form-group',[
						'name' => 'name',
						'type' => 'text',
						'value' => $product->name,
						'classes' => ['form-control', 'form-control-sm']
					])

					{{-- image --}}
					@include('products.partials.form-group',[
						'name' => 'image',
						'type' => 'file',
						'classes' => ['form-control-file', 'form-control-sm']
					])

					
					{{-- end category_id --}}
					<label for="product_status_id">Unit Status</label>
					<select name="product_status_id" id="product_status_id" class="form-control form-control-sm">
						@foreach($product_statuses as $product_status)
							<option value="{{$product_status->id}}" {{$product_status->id === $product->product_status_id ? "selected" : ""}}>
								{{$product_status->name}}
							</option>
						@endforeach
						
					</select>

					{{-- description --}}
					<label class="mt-3" for="description">Unit Description:</label>
					<textarea 
						name="description" 
						id="description" cols="30" 
						rows="10" 
						class="form-control 
							form-control-sm 
							@error('description') is-invalid @enderror
						"
						
						>{{$product->description}}</textarea>
					@error('description')
					<small class="d-block invalid-feedback">
						<strong>
							{{ $message }}
						</strong>
					</small>
					@enderror

					
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button class="btn btn-secondary btn-warning my-2">Edit Unit</button>
					</div>
					

				</form>
			
      </div>
      
    </div>
  </div>
</div>