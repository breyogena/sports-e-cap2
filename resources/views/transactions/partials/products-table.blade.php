<div class="row">
	<div class="col-12">
		{{-- table start --}}
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Name</th>
						<th>Asset type</th>
					</tr>
				</thead>
				<tbody>
					{{-- product row start --}}
					@foreach($transaction->products as $product)
					<tr>
						<td>{{$product->product_code}}</td>
						<td>{{$product->category->name}}</td>
					</tr>
					@endforeach
					{{-- product row end --}}
				</tbody>
			</table>
		</div>
		{{-- table end --}}
	</div>
</div>