<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;


class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $this->authorize('viewAny', $cart);

        if (session()->has('cart')){

            $products = [];

            foreach (session('cart') as $id => $value) {

                $product = Product::find($id);

                $product->quantity = session("cart.$id");

                $products[] = $product;
                # code...
            }

	        // $products = Product::find(array_keys(session('cart')));
	        return view('carts.index')
	            ->with('products', $products);
        } else {
            return view('carts.index');

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $this->authorize('update', $cart);
        $product = $id;
        $request->session()->put("cart.$id", $product);
        return redirect( route('cart.index'))
         ->with('success',"Unit added in form successfully!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        session()->forget("cart.$id");

        // check if session cart is empty
        if (count(session()->get('cart')) === 0) {
            session()->forget('cart');
        }
        
        return back()
        ->with('success',"Unit remove in form successfully!");
        ;
    }

    public function clearAll()
    {
        session()->forget('cart');

        return back()
        ->with('success',"Request form is empty!");
        
    }
}
