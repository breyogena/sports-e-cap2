@extends('layouts.app')

@section('content')
<div class="container my-5">
    <h1 class="text-center text-uppercase text-decoration">Dashboard</h1>
    <div class="row justify-content-center">

        <div class="col-md-8">
            @cannot('isAdmin')
            <div class="card">
                
                <div class="card-header">{{ __('Welcome') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{-- {{('You are logged in! Welcome to Sports-E Collections.' ) }} --}}
                    <h5 class="text-center">You are now logged in!</h5>
                    <h5 class="text-center">Welcome to Sports-E Collections. Rent now, Return later!</h5>
                    @include('sweetalert::alert')
                </div>
            </div>
                @endcan
        </div>
        @can('isAdmin')
            <div class="col-12 col-md-6">
                <div class="card text-center shadow p-3 mb-5 bg-white rounded">
                    <div class="card-body">
                        <svg width="3em" height="3.5em" viewBox="0 0 16 16" class="bi bi-archive-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                          <path fill-rule="evenodd" d="M12.643 15C13.979 15 15 13.845 15 12.5V5H1v7.5C1 13.845 2.021 15 3.357 15h9.286zM6 7a.5.5 0 0 0 0 1h4a.5.5 0 0 0 0-1H6zM.8 1a.8.8 0 0 0-.8.8V3a.8.8 0 0 0 .8.8h14.4A.8.8 0 0 0 16 3V1.8a.8.8 0 0 0-.8-.8H.8z"/>
                      </svg>
                        <h5 class="card-title font-weight-bold my-2">Pending Requests</h5>
                        <p class="card-text font-weight-bold">
                            {{ collect($transactions)->where('request_status_id', 1)->count()}}
                        </p>
                        {{-- <a href="{{route('transactions.index')}}" class="btn btn-primary">Go to transaction history.</a> --}}
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="card text-center shadow p-3 mb-5 bg-white rounded">
                    <div class="card-body">
                        <svg width="4em" height="3.5em" viewBox="0 0 16 16" class="bi bi-people-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                          <path fill-rule="evenodd" d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-5.784 6A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216zM4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z"/>
                      </svg>
                        <h5 class="card-title font-weight-bold my-2">Total of registered user.</h5>
                        <p class="card-text font-weight-bold">
                            {{ collect($users)->where('role_id', 2)->count()}}
                        </p>
                       {{--  <a href="" class="btn btn-primary">Go to transaction history.</a> --}}
                    </div>
                </div>
            </div>
        @endcan
    </div>
</div>
@endsection
