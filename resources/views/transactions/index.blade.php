@extends('layouts.app')
@section('content')
	<div class="container my-3">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					All Requests
				</h1>
			</div>
		</div>

		{{-- <div class="row">
  <div class="col-10 col-md-4 mb-3 mb-md-0 mx-auto mx-md-0">
    <div class="list-group" id="list-tab" role="tablist">
      <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">
      	All
      </a>
      <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">
      	Pending
      </a>
      <a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">
      	Approved
      </a>
      <a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings">
      	Decline
      </a>
      <a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings">
      	Completed
      </a>
    </div>
  </div>
  <div class="col-10 col-md-8  mx-auto mx-md-0">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
      	@include('transactions.partials.accordion')
      </div>
      <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
      	@include('transactions.partials.accordion')
      </div>
      <div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">...</div>
      <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">...</div>
    </div>
  </div>
</div> --}}
		
		{{-- transactions section start --}}
		<div class="row">
			{{-- @foreach($transactions as $transaction) --}}
				<div class="col-11 mx-auto mx-md-0">
					@include('transactions.partials.accordion')
				</div>
			{{-- @endforeach --}}
		</div>
		{{-- transactions section end --}}

	</div>
@endsection