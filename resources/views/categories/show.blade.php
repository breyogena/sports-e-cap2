@extends('layouts.app')

@section('content')
<div class="container my-4">
	<div class="row">
		<div class="col-12 col-md-4 col-lg-3 mx-auto">
			@include('categories.partials.card', ['view' => false])

			@includeWhen(Session::has('message'), 'partials.alert')
			
			@can('isAdmin')
			<div class="my-3">
				<form 
					action="{{ route('products.store') }}" 
					method="post" 
					enctype="multipart/form-data"

				>
					@csrf
					
					<?php $product_code = strtoupper(Str::random(10)) ?>

					<h5 class="text-center">Create Unit</h5>

					<label for="product_code">Unit Code:</label>
					<div class="input-group mb-3">

						<div class="input-group-prepend">
							<span class="input-group-text" id="product_code">{{$category->id}}-</span>
						</div>
						<input type="text" id="product_code" name="product_code" value="{{$product_code}}" class="form-control" placeholder="{{$product_code}}" aria-label="product_code" aria-describedby="basic-addon1">
					</div>



					{{-- name --}}
					@include('products.partials.form-group',[
							'name' => 'name',
							'type' => 'text',
							'classes' => ['form-control', 'form-control-sm']
					])


					{{-- category id--}}
					<label for="category_id">Unit Category:</label>
					<select name="category_id" id="category_id" class="form-control form-control-sm">
						<option value="{{ $category->id }}">{{ $category->name }}</option>
					</select>
					

					{{-- image --}}
					@include('products.partials.form-group',[
							'name' => 'image',
							'type' => 'file',
							'classes' => ['form-control-file', 'form-control-sm']
					])

					{{-- description --}}
					<label class="mt-3" for="description">Unit Description:</label>
					<textarea 
						name="description" 
						id="description" cols="30" 
						rows="10" 
						class="form-control 
							form-control-sm 
							@error('description') is-invalid @enderror
						"
						
						></textarea>
					@error('description')
					<small class="d-block invalid-feedback">
						<strong>
							{{ $message }}
						</strong>
					</small>
					@enderror
					<button class="btn btn-sm btn-primary mt-3 w-100">
						<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-folder-plus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							<path fill-rule="evenodd" d="M9.828 4H2.19a1 1 0 0 0-.996 1.09l.637 7a1 1 0 0 0 .995.91H9v1H2.826a2 2 0 0 1-1.991-1.819l-.637-7a1.99 1.99 0 0 1 .342-1.31L.5 3a2 2 0 0 1 2-2h3.672a2 2 0 0 1 1.414.586l.828.828A2 2 0 0 0 9.828 3h3.982a2 2 0 0 1 1.992 2.181L15.546 8H14.54l.265-2.91A1 1 0 0 0 13.81 4H9.828zm-2.95-1.707L7.587 3H2.19c-.24 0-.47.042-.684.12L1.5 2.98a1 1 0 0 1 1-.98h3.672a1 1 0 0 1 .707.293z"/>
							<path fill-rule="evenodd" d="M13.5 10a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1H13v-1.5a.5.5 0 0 1 .5-.5z"/>
							<path fill-rule="evenodd" d="M13 12.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0v-2z"/>
						</svg>
						Add Unit
					</button>
				</form>
			</div>
			@endcan                      
		</div>
		<div class="col-12 col-md-8 col-lg-9">
			<div class="row">
				@foreach($products as $product)
					@if($category->id === $product->category_id )
					{{-- product card start --}}
						<div class="col-12 col-sm-6 col-lg-4 my-3">
							<div class="card mb-2">
								<img src="{{ asset($product->image) }}" width="100" height="250" alt="" class="card-img-top">
								<div class="card-body">
									<h5 class="card-title">
										{{ $product->name}}
									</h5>
									<p class="card-text mb-0">
										Unit Code: 
										{{$product->product_code}}
									</p>
									<p class="{{-- font-weight-bold --}}">Status: {{$product->product_status->name}}</p>
									<p class="card-text mb-0">
										<span class="badge badge-danger text-white">
											{{ $product->category->name }}
										</span>
										{{-- <span class="badge badge-{{$product->product_status_id === 1 ? "danger" : ($product->product_status_id === 2? "info" : "success")}}">
											{{$product->product_status->name}}
										</span> --}}
									</p>
								</div>
								@can('isAdmin')
									<div class="card-footer">
										@include('products.partials.edit-btn')
										@include('products.partials.delete-form')
									</div>
								@endcan
							</div>
						</div>
					@endif
				@endforeach
		
			</div>
		</div>
	</div>
</div>
@endsection