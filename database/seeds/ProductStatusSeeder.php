<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_statuses')->insert([
        	'name' => 'Available'
        ]);

        DB::table('product_statuses')->insert([
        	'name' => 'Unavailable'
        ]);

         DB::table('product_statuses')->insert([
        	'name' => 'Under maintenance'
        ]);
    }
}
