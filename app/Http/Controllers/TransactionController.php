<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Product;
use App\Product_status;
use App\Request_status;
use Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Transaction::class);
        $transactions = Transaction::all();
        $request_statuses = Request_status::all();

        return view('transactions.index')
            ->with('transactions', $transactions)
            ->with('request_statuses', $request_statuses);
             // ->with('success', 'Request is added successfully. Please wait for confimation of your request.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Transaction::class);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Transaction::class);
        $validatedData = $request->validate([
            'date_needed' => 'required|',
            'date_return' => 'required|after:date_needed'
            // 'created_at' => 'required|before:date_needed'
        ]);

        // dd($request);

        // get the details needed
        $transaction = new Transaction;
        $transaction->transaction_code = strtoupper(Str::random(10));
        $transaction->user_id = Auth::user()->id;
        $transaction->date_needed = $request->date_needed;
        $transaction->date_return = $request->date_return;
        $transaction->save();


        $products = Product::find(array_keys(session('cart')));

        foreach($products as $product) {

          $product_id = $product->id;

          $transaction_id = $transaction->id;

          $transaction->products()->attach($product->id,[
            'transaction_id' => $transaction_id,
            'product_id' => $product_id
          ]);

        }
        

        session()->forget('cart');

        $transaction->save();



        return redirect( route('transactions.show', $transaction->id))
            ->with('success', 'Request has been completed. Please wait for the confimation of your request product.');
           
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        $this->authorize('view', $transaction );
        $request_statuses = Request_status::all();
         return view('transactions.show')
            ->with('transaction', $transaction)
            ->with('request_statuses', $request_statuses);
            // ->with('success', 'Request is added successfully. Please wait for confimation of your request.');
             // ->with('product_statuses', $product_statuses);
            // ->with('request_statuses', Request_status::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        $this->authorize('update', Transaction::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {   
        // dd($transaction);

        // $validatedData = $request->validate([
        //     "request_status_id" => 'nullable'
        // ]);
        $this->authorize('update', $transaction );
        $transaction->request_status_id = $request->request_status_id;

        $transaction->save();
        $data = DB::table('product_transaction')->get();//name of pivot table

        // $products = Product::all();

        foreach ($data as $product_transaction) {
            $transaction_id = $product_transaction->transaction_id;

            $product_id = $product_transaction->product_id;

            $items = DB::table('transactions')->where('id', $transaction_id)->get();

            foreach ($items as $item) {
                if($item->request_status_id === 2){
                    $products = DB::table('products')
                    ->where('id', $product_id)->get();
                    foreach ($products as $product) {
                        $product = DB::table('products')
                        ->where('id', $product_id)
                        ->update(['product_status_id' => 2]);
                    }
                }
            }

            if($item->request_status_id !== 2){
                    $products = DB::table('products')
                    ->where('id', $product_id)->get();
                    foreach ($products as $product) {
                        $product = DB::table('products')
                        ->where('id', $product_id)
                        ->update(['product_status_id' => 1]);
                    }
                }
        }

            // if ($transaction->request_status_id !== 1) {
            //     foreach ($transaction->products as $product) {

            //         $products = DB::table('products')->where('product_status_id', $product->id)->get();
            //     }
            // } else {
            //     foreach ($transaction->products as $product) {
            //          $products = DB::table('products')->where('product_status_id', $product->id)->get();
            //     }
            // }
        

        // $transaction->update($validatedData);
        // $transaction->save();

        return back()
            ->with('success', "Request status updated!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
