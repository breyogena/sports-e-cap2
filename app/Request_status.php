<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request_status extends Model
{
    public function transactions()
    {
    	return $this->hasMany('App\Transaction');
    }
}
