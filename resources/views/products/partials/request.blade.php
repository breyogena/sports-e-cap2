<form action="{{route('cart.update', $product->id)}}" method="post" class="my-2">
	@csrf
	@method('PUT')
	

	{{-- <button type="button" class="btn btn-success w-100" data-toggle="modal" data-target="#staticBackdrop{{$product->id}}" 
		{{$product->product_status_id !== 1 ? "disabled" : ""}}>
  		Rent
	</button> --}}

	<button type="button" class="btn btn-{{$product->product_status_id === 1 ? "success" : "secondary"}} w-100" data-toggle="modal" data-target="#staticBackdrop{{$product->id}}" 
		{{$product->product_status_id !== 1 ? "disabled" : ""}}>
  		{{$product->product_status_id !== 1 ? "Unavailable" : "Rent"}}
	</button>
<!-- Modal -->
<div class="modal fade" id="staticBackdrop{{$product->id}}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Full details below:</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">    
		  	<div class="d-flex">
				<div class="col-5">
					<img src="{{ $product->image }}" alt="" class="w-100">
				</div>
				<div class="col-7">
							
					{{-- description start --}}
					<div class="row d-block">
						<h5 class="font-weight-bold text-uppercase">{{ $product->name}}</h5>
						<div class="font-weight-bold">Description:</div>
						<div>{{$product->description}}</div>
					</div>
					{{-- description end --}}

					{{-- category start --}}
					<div class="row">
						<div class="font-weight-bold">Item: </div>
						<div>
							<span class="badge badge-success text-white">
								{{ $product->category->name }}
							</span>
						</div>
					</div>
					{{-- category end --}}
				</div>
			</div>
      	</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button class="btn btn-secondary btn-success mt-1">Rent</button>
      </div>
    </div>
  </div>
</div>

</form>

