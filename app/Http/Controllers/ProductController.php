<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\Product_status;
use Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Product::class);
        $products = Product::all()->sortBy('name');
        $categories = Category::all()->sortBy('name');

        // total of all products in category
        // foreach ($categories as $category) {
        //     $category->total = DB::table('products')
        //     ->where('category_id', $category->id)->count();
        //     $category->save();
        // }

        // stocks 
        // foreach ($categories as $category) {
        //     $category->available = DB::table('products')
        //     ->where('category_id', $category->id)
        //     ->where('products.product_status_id', 1)->count();
        //     $category->save();
        // }

        return view('products.index')
            ->with('products', $products)
            ->with('product_statuses', Product_status::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Product::class);
        return view('products.create')
            ->with('categories', Category::all()->sortBy('name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $this->authorize('create', Product::class);
        $validatedData = $request->validate([
            // 'code' => 'required',
            'name' => 'required|string',
            'product_code' => 'required|min:10',
            'category_id' => 'required',
            'image' => 'required|image|max:2000',
            'description' => 'required'
        ]);

        // $product->product_code = strtoupper(Str::random(10));

        // $path = $request->file('image')->store('public/products');

        // $url = Storage::url($path);

        $product = new Product($validatedData);
        
        $path = $request->file('image');
        $path_name = time().".".$path->getClientOriginalExtension();
        $destination = "image/"; // automatic create folder in public folder
        $path->move($destination, $path_name);
        $product->image = $destination.$path_name;

        // $product->image = $url;

        $product->save();

        return back()
            ->with('success', 'Unit is added successfully!');
            // ->with('category', $category);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $products = Product::all();
        $count = collect($products)->where('category_id', $category->id)
            ->where('product_status_id', 1)->count();
        $count_total = collect($products)->where('category_id', $category->id)->count();
         return view('products.index')
            ->with('count', $count)
            ->with('count_total', $count_total)
            ->with('category', $category)
            ->with('products', Product::all()->where('category_id', $category->id))
            ->with('product_statuses', Product_status::all());

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $this->authorize('update', $product);
        return view('products.edit')
            ->with('product', $product)
            ->with('categories', Category::all())
            ->with('product_statuses', Product_status::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $this->authorize('update', $product);
        $validatedData = $request->validate([
            'name' => 'required|string',
            // 'product_status_id' => 'required',
            // 'product_code' => 'required',
            'image' => 'image|max:2000',
            'description' => 'required'
        ]);

        $product->product_status_id = $request->product_status_id;
        // update product
        $product->update($validatedData);


        $path = $request->file('image');

        if ($path != "") {
        
            // store the image
             // $path = $request->file('image')->store('public/products');

            // this will return the url of the saved image
             // $url = Storage::url($path);

             // set the new url of image
             // $product->image = $url;

            $path_name = time().".".$path->getClientOriginalExtension();
            $destination = "image/"; // automatic create folder in public folder
            $path->move($destination, $path_name);
            $product->image = $destination.$path_name;

        }
        // save product
        $product->save();

        return back()
            ->with('success', 'Unit is updated successfully!')
            ->with('product_statuses', Product_status::all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->authorize('delete', $product);
        $product->delete();
        return redirect(route('products.index'))
            ->with('success', "Unit {$product->name} has been deleted!")
            ->with('alert', 'warning');
    }
}
