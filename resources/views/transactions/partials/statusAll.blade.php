<div class="accordion" id="accordionExample">

@cannot('isAdmin')
  @foreach($transactions as $transaction)
    {{-- transaction card start --}}
      @if($transaction->user_id === Auth::user()->id)
        <div class="card">
          <div class="card-header" id="headingOne">
            <h2 class="mb-0">
              <button class="btn btn-link btn-block text-left text-dark" type="button" data-toggle="collapse" data-target="#collapse{{$transaction->id}}" aria-expanded="true" aria-controls="collapseOne">
                 {{$transaction->transaction_code}} | {{ucfirst($transaction->user->name)}}
                <span class="badge 
                  badge-{{$transaction->request_status_id === 1? "warning" :
          ($transaction->request_status_id === 2 ? "primary" :
          ($transaction->request_status_id === 3 ? "danger" : "success"))

        }}">
                 {{$transaction->request_status->name}}
                </span>
              </button>
            </h2>
          </div>
          <div id="collapse{{$transaction->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
               @include('transactions.partials.summary')
            </div>
          </div>
        </div>
      {{-- transaction card start --}} 
    @endif
  @endforeach
@endcannot

@can('isAdmin')
  @foreach($transactions as $transaction)
    {{-- transaction card start --}}
      <div class="card">
        <div class="card-header" id="headingOne">
          <h2 class="mb-0">
            <button class="btn btn-link btn-block text-left text-dark" type="button" data-toggle="collapse" data-target="#collapse{{$transaction->id}}" aria-expanded="true" aria-controls="collapseOne">
               {{$transaction->transaction_code}} | {{ucfirst($transaction->user->name)}}
                <span class="badge badge-{{$transaction->request_status_id === 1? "warning" :
                ($transaction->request_status_id === 2 ? "primary" :
                ($transaction->request_status_id === 3 ? "danger" : "success"))

                }}">
               {{$transaction->request_status->name}}
              </span>
            </button>
          </h2>
        </div>
        <div id="collapse{{$transaction->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
          <div class="card-body">
             @include('transactions.partials.summary')
          </div>
        </div>
      </div>
    {{-- transaction card start --}} 
  @endforeach
@endcan

  
</div>