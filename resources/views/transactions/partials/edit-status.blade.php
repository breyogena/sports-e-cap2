@can('isAdmin')
<form action="{{ route('transactions.update', $transaction->id )}}" method="post" class=" border p-3">
	@csrf
	@method("PUT")
	<label for="request_status_id">Edit:</label>
	<select name="request_status_id" id="request_status_id" class="form-control form-control-sm">
		@foreach($request_statuses as $request_status)
			<option value="{{$request_status->id}}" {{$request_status->id === $transaction->request_status_id ? "selected" : ""}}>{{$request_status->name}}</option>
		@endforeach
	</select>
	<button class="btn btn-sm btn-outline-primary my-1">Edit</button>
</form>
@endcan