<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = ["request_status_id"];
    
    public function request_status()
    {
    	return $this->belongsTo('App\Request_status');
    }

    public function products()
    {
    	return $this->belongsToMany('App\Product')
    		->withTimestamps();
    }

    public function product_status(){
        return $this->belongsTo('App\Product_status');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

}
