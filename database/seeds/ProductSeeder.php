<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;



class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
        	'name' => 'Basketball Gear',
        	'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet in maiores ipsum, eaque at voluptatum ipsa. Non ullam, tenetur earum.',
        	'image' => 'https://www.academy.com/content/dam/academysports/misc-product-images/nike-bball-package-4.jpg?$d-plp-top-categories$',
            'product_code' => strtoupper(Str::random(10)),
        	'category_id' => 1
        ]);

         DB::table('products')->insert([
            'name' => 'Basketball Ball',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet in maiores ipsum, eaque at voluptatum ipsa. Non ullam, tenetur earum.',
            'image' => 'https://cdn11.bigcommerce.com/s-2sxhiat0li/products/182/images/588/GL7X_AD__99451.1461966775.500.750.jpg?c=2',
            'product_code' => strtoupper(Str::random(10)),
            'category_id' => 1
        ]);

        DB::table('products')->insert([
        	'name' => 'Volleyball Gear',
        	'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet in maiores ipsum, eaque at voluptatum ipsa. Non ullam, tenetur earum.',
        	'image' => 'https://images.homedepot-static.com/productImages/48fbf49b-6eee-4e07-afff-cf60425b3d87/svn/dunlop-volleyball-equipment-ne100y19011-64_400_compressed.jpg',
            'product_code' => strtoupper(Str::random(10)),
        	'category_id' => 2
        ]);

        DB::table('products')->insert([
            'name' => 'Volleyball Ball',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet in maiores ipsum, eaque at voluptatum ipsa. Non ullam, tenetur earum.',
            'image' => 'https://mikasasports.com/wp-content/uploads/2015/04/MVA2001.png',
            'product_code' => strtoupper(Str::random(10)),
            'category_id' => 2
        ]);

        DB::table('products')->insert([
            'name' => 'Boxing Gear',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet in maiores ipsum, eaque at voluptatum ipsa. Non ullam, tenetur earum.',
            'image' => 'https://images-na.ssl-images-amazon.com/images/I/71UDYdu%2B07L.jpg',
            'product_code' => strtoupper(Str::random(10)),
            'category_id' => 3
        ]);

        DB::table('products')->insert([
            'name' => 'Boxing Gloves',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet in maiores ipsum, eaque at voluptatum ipsa. Non ullam, tenetur earum.',
            'image' => 'https://contents.mediadecathlon.com/p1592998/k$c901b69179d40a0c2d013d8880a994f7/beginner-boxing-gloves-100-red.jpg?&f=800x800',
            'product_code' => strtoupper(Str::random(10)),
            'category_id' => 3
        ]);

        DB::table('products')->insert([
            'name' => 'Badminton Gear',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet in maiores ipsum, eaque at voluptatum ipsa. Non ullam, tenetur earum.',
            'image' => 'https://www.thesun.co.uk/wp-content/uploads/2019/11/SM-COMP-BANDMI-1.jpg',
            'product_code' => strtoupper(Str::random(10)),
            'category_id' => 4
        ]);

        DB::table('products')->insert([
            'name' => 'Badminton Racket',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet in maiores ipsum, eaque at voluptatum ipsa. Non ullam, tenetur earum.',
            'image' => 'https://images-na.ssl-images-amazon.com/images/I/71Qtd5kYkfL._AC_SL1500_.jpg',
            'product_code' => strtoupper(Str::random(10)),
            'category_id' => 4
        ]);

        

        

    }
}
