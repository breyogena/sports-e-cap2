<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\User;
use App\Role;
use App\Request_status;

class DashboardController extends Controller
{
    public function index()
    {
        $users = User::all();
    	$roles = Role::all();
    	foreach($users as $user) {

    		if($user->role_id != 1){
		       
		        return view('home')
            		->with('transactions', Transaction::all())
            		->with('users', User::all());

		    } else {

            		 return view('dashboard')
		            ->with('transactions', Transaction::all())
		            ->with('users', User::all());
		    }
    	}


    
    }
}
