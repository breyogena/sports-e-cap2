<div class="accordion" id="accordionExample">

@cannot('isAdmin')
  @foreach($transactions as $transaction)
      @if($transaction->user_id === Auth::user()->id) 
        @if($transaction->request_status_id === 4)
          @include('transactions.partials.list-group')
        @endif
      @endif
  @endforeach
@endcannot

@can('isAdmin')
  @foreach($transactions as $transaction)
    @if($transaction->request_status_id === 4)
          @include('transactions.partials.list-group')
    @endif
  @endforeach
@endcan

  
</div>