@extends('layouts.app')
@section('content')

	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 col-lg-6 mx-auto">
				<h1 class="text-center">Edit Unit</h1>
			</div>
		</div>
		@include('sweetalert::alert')

		{{-- edit form start --}}
		<div class="row">
			<div class="col-12 col-md-8 col-lg-6 mx-auto">
				<form 
					action="{{ route('products.update', $product->id)}}"
					method="post" 
					enctype="multipart/form-data" 
				>
					@csrf
					@method('PUT')

					{{-- name --}}
					@include('products.partials.form-group',[
						'name' => 'name',
						'type' => 'text',
						'value' => $product->name,
						'classes' => ['form-control', 'form-control-sm']
					])

					{{-- image --}}
					@include('products.partials.form-group',[
						'name' => 'image',
						'type' => 'file',
						'classes' => ['form-control-file', 'form-control-sm']
					])
					

					{{-- start category_id --}}
					<label for="category_id">Unit item</label>
					<select name="category_id" id="category_id" class="form-control form-control-sm">
						@foreach($categories as $category)
							<option 
								value="{{$category->id}}"
								{{ $category->id === $product->category_id ? "selected" : ""}}
							>
								{{$category->name}}
							</option>
						@endforeach
						
					</select>
					@error('category_id')
						<small class="d-block invalid-feedback">
							<strong>							
								{{ $message }}
							</strong>
						</small>
					@enderror

					
					{{-- end category_id --}}
					<label for="product_status_id">Unit Status</label>
					<select name="product_status_id" id="product_status_id" class="form-control form-control-sm">
						@foreach($product_statuses as $product_status)
							<option value="{{$product_status->id}}">
								{{$product_status->name}}
							</option>
						@endforeach
						
					</select>

					{{-- description --}}
					<label class="mt-3" for="description">Unit Description:</label>
					<textarea 
						name="description" 
						id="description" cols="30" 
						rows="10" 
						class="form-control 
							form-control-sm 
							@error('description') is-invalid @enderror
						"
						
						>{{$product->description}}</textarea>
					@error('description')
					<small class="d-block invalid-feedback">
						<strong>
							{{ $message }}
						</strong>
					</small>
					@enderror

					<button class="btn btn-sm btn-warning my-2">Edit Unit</button>
					

				</form>
			</div>
		</div>
		{{-- edit form end --}}
	</div>

@endsection