<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Product_status;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Category::class);
        $categories = Category::all()->sortBy('name');
        $products = Product::all()->sortBy('name');

        // foreach ($categories as $category) {
        //     $category->total = DB::table('categories')
        //         ->where('category_id', $category->id)
        //         ->count();
        //     $category->save();
        // }

        // foreach ($categories as $category) {
        //     $category->available = DB::table('categories')
        //         ->where('category_id', $category->id)
        //         ->where('product_statuses_id', 1)
        //         ->count();
        //     $category->save();
        // }        


        return view('categories.index')
            ->with('products', $products)
            ->with('categories', $categories);
            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Category::class);
        return view('categories.index');

        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Category::class);
        $validatedData = $request->validate([
            'name' =>'required|unique:categories,name'
        ]);

        $category = new Category($validatedData);
        $category->save();

        return redirect(route('categories.index'))
            ->with('success',"Item $category->name is added successfully")
            ->with('alert','warning');
            // ->with('product', $product);

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $this->authorize('view', $category);

        $products = Product::all();
        $count = collect($products)->where('category_id', $category->id)
            ->where('product_status_id', 1)->count();
        $count_total = collect($products)->where('category_id', $category->id)->count();

        $products = Product::all();
         return view('categories.show')
            ->with('count', $count)
            ->with('count_total', $count_total)
            ->with('category', $category)
            ->with('products', Product::all()->where('category_id', $category->id))
            ->with('product_statuses', Product_status::all());
             // ->with('products', $products);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $this->authorize('update', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->authorize('update', $category);
         $validatedData = $request->validate([
            'name' =>'required|unique:categories,name'
        ]);
        $category->update($validatedData);
        $category->save();


        return redirect(route('categories.index', $category->id))
            ->with('success', "Item $category->name is updated successfully.");
            // ->with('product', $product);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $this->authorize('delete', $category);

        $products = Product::all();
        $count_total = collect($products)->where('category_id', $category->id)->count();
        if($count_total === 0) {
            $category->delete();
            return redirect(route('categories.index'))
                ->with('success',"Item $category->name has been deleted")
                ->with('alert','warning');
        }
            return back()
                ->with('success', 'Item has a product left, cannot be deleted.');

        //

    }
}
