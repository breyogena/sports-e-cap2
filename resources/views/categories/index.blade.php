@extends('layouts.app')

@section('content')
	<div class="container-fluid py-5" id="category-index">
		{{-- alert-message --}}
		@includeWhen(Session::has('message'),'partials.alert')
		{{-- end alert message --}}
		
		<div class="row">
		@can('isAdmin')
		  <div class="col-4">
			    <div class="list-group" id="list-tab" role="tablist">
			      <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">All items</a>
			      	<a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">Create new item</a>
			    </div>
		  </div>
		@endcan
		<div class="col-8 mx-auto">
		    <div class="tab-content" id="nav-tabContent">
		      <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
		      	<div class="row">
					
					@foreach($categories as $category)
						<?php $count = collect($products)->where('category_id', $category->id) 
							->where('product_status_id', 1)->count();
							$count_total = collect($products)->where('category_id', $category->id)
							->count();

						?>
						{{-- card start --}}
						<div class="col-12 col-sm-6 col-md-4 col-lg-3">
							@include('categories.partials.card')
						</div>
						{{-- card end --}}
					@endforeach
					@include('sweetalert::alert')
				</div>
			</div>
		      <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
		      	<div class="container">
				<div class="row">
					<div class="col-12">
						<h1 class="text-center text-white">
							Create New Item
						</h1>
					</div>
				</div>

				{{-- start form section --}}
				<div class="row">
					<div class="col-12 col-sm-6 mx-auto">
						<form action="{{ route('categories.store') }}" method="post">
							@csrf
							<label for="name" class="text-white font-weight-bold">Name:</label>
							<input type="text" name="name" id="name" required class="form-control form-control-sm">
							<button class="btn btn-sm btn-primary mt-1">Add Item</button>
						</form>
					</div>
					@include('sweetalert::alert')
				</div>
				{{-- end form section --}}
			</div>
		      </div>
		      <div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">...</div>
		      <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">...</div>
		    </div>
		  </div>
		</div>



		{{-- category list start --}}
		
		{{-- category list end --}}
	</div>
@endsection
