@extends('layouts.app')

@section('content')
	<div class="container my-3" id="product-index">
		{{-- header start --}}
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					All Units
				</h1>
			</div>
		</div>
		{{-- header end --}}
		@include('sweetalert::alert')

		{{-- alert message --}}
		@includeWhen(Session::has('message'), 'partials.alert')
		

		{{-- products section start --}}
		<div class="row">

			@foreach($products as $product)
				<div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-2 ">
					{{-- product card start --}}
					<div class="card">
						<img src="{{ $product->image }}"  alt="" class="card-img-top">
						<div class="card-body">
							<h5 class="card-title font-weight-normal text-center">
								{{ $product->name}}
							</h5>
							<p class="card-text mb-0 font-weight-normal text-center">
								Unit Code: 
								{{$product->product_code}}
							</p>
							<p class="font-weight-normal text-center">Status: {{$product->product_status->name}}</p>
							<p class="card-text mb-0">
								<span class="badge badge-success text-white">
									{{ $product->category->name }}
								</span>
								{{-- <span class="badge badge-{{$product->product_status_id === 1 ? "success" : ($product->product_status_id === 2? "danger" : "warning")}}">
									{{$product->product_status->name}}
									
								</span> --}}

							</p>

							

							@cannot('isAdmin')
								@include('products.partials.request')
							@endcannot
							

							
						</div>
						@can('isAdmin')
							<div class="card-footer">
								@include('products.partials.edit-btn')
								@include('products.partials.delete-form')
							</div>
						@endcan
					</div>
					{{-- product card end --}}
				</div>
			@endforeach
		</div>
		{{-- products section end --}}
	</div>
@endsection

