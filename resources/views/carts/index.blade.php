@extends('layouts.app')
@section('content')
    <div class="container my-4">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center">
                    Request form
                </h1>
            </div>
        </div>

        @include('sweetalert::alert')

        @if(!Session::has('cart'))
            {{-- alert start --}}
            <div class="alert alert-info alert-dismissible fade show text-center" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong>Request form is empty!</strong> 
            </div>
        {{-- alert end --}}
        @else
            <div class="row">
                <div class="col-12">
                    {{-- start cart table --}}
                    <table class="table-responsive">
                        <table class="table table-hover text-center">
                            <thead>
                                <tr>
                                    <th>Unit Name</th>
                                    <th>Unit Code</th>
                                    <th>Unit Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- start item row --}}
                                @foreach($products as $product)
                                    <tr>
                                        <td scope="row">{{$product->name}}</td>
                                        <td scope="row">{{$product->product_code}}</td>
                                        <td scope="row">{{$product->product_status->name}}</td>
                                        <td>
                                            <form action="{{route('cart.destroy', $product->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-sm btn-outline-danger">Remove</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                {{-- end item row --}}
                            </tbody>
                        </table>
                    </table>
                    {{-- end cart table --}}
                    <div class="row">
                      <div class="col-12 col-sm-8 mx-auto text-center">
                            @guest
                              <a href="{{ route('login')}}" class="btn btn-sm btn-success my-1">Login to checkout</a>
                            @else
                          
                            <form action="{{ route('transactions.store') }}" method="post" class="mb-5">
                              @csrf
                              <div class="d-flex justify-content-between">
                                  <div class="container align-items-center">
                                      <div class="col align-self-start">
                                          <div class="form-group">
                                              <label for="date_needed">Date needed:</label>
                                              <input required type="date" 
                                                min="<?= date('Y-m-d',strtotime("+1 day")); ?>"
                                                max="<?= date('Y-m-d',strtotime("+30 day")); ?>" 
                                                name="date_needed" 
                                                id="date_needed" 
                                                class="form-control form-control-sm 
                                                @error('date_needed') is-invalid @enderror w-100" 
                                                placeholder="Date Needed" 
                                                aria-describedby="date_needed"
                                              >
                                              <small id="date_needed" class="text-muted mx-auto">Date when you need this product</small>
                                               @error('date_needed')
                                              <small class="d-block invalid-feedback">
                                                <strong>
                                                  {{ $message }}
                                                </strong>
                                              </small>
                                              @enderror
                                          </div>
                                      </div>
                                  </div>
                                  <div class="container align-items-center">
                                      <div class="col align-self-start">
                                          <div class="form-group">
                                              <label for="date_return">Date return:</label>
                                              <input required type="date"
                                                min="<?= date('Y-m-d',strtotime("+3 day")); ?>" 
                                                max="<?=date('Y-m-d',strtotime("+30 day"));?>" 
                                                name="date_return" id="date_return" 
                                                class="form-control form-control-sm 
                                                @error('date_return') is-invalid @enderror w-100 " 
                                                placeholder="Date Return" 
                                                aria-describedby="date_return"
                                              >
                                              <small id="date_return" class="text-muted">Date when you return product</small>
                                              @error('date_return')
                                              <small class="d-block invalid-feedback">
                                                <strong>
                                                  {{ $message }}
                                                </strong>
                                              </small>
                                              @enderror
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <button class="btn btn-sm btn-success">
                                Submit form
                              </button>
                          </form>
                          @endguest
                      </div>
                  </div>
                </div>
            </div>
            {{-- clear cart --}}
            <div class="row">
                <div class="col-12 text-left">
                    <form action="{{route('cart.clearAll')}}" method="post">
                        @csrf
                        @method('DELETE')
                        <div class="d-block mb-3">
                          <a href="{{ route('products.index') }}" class="btn btn-sm btn-success">
                            Add more units
                          </a>
                        </div>
                        <button class="btn btn-sm btn-outline-danger">Clear form</button>
                    </form>

                </div>
            </div>
        @endif
    </div>
    
@endsection