<div class="table-responsive">
	<table class="table table-hover">

		@include('sweetalert::alert')
		
		{{-- transacation code --}}
		<tr>
			<td class="font-weight-bold"> 
				<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-calendar3-week" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
					<path fill-rule="evenodd" d="M14 0H2a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z"/>
					<path fill-rule="evenodd" d="M12 7a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-5 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm2-3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-5 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
				</svg>	
				Transaction Date:
			</td>
			<td>{{date("M d, Y - g:i A", strtotime($transaction->created_at)) }}</td>
		</tr>
		{{-- transaction code end --}}
		
		{{-- Customer name start --}}
		<tr>
			<td class="font-weight-bold">
				<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
					<path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
					<path fill-rule="evenodd" d="M2 15v-1c0-1 1-4 6-4s6 3 6 4v1H2zm6-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
				</svg>
				Username:
			</td>
			<td>{{ucfirst($transaction->user->name) }}</td>
		</tr>
		{{-- Customer name end --}}

		{{-- Status start --}}
		<tr>
			<td class="font-weight-bold">
				<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-card-text" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
					<path fill-rule="evenodd" d="M14.5 3h-13a.5.5 0 0 0-.5.5v9a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z"/>
					<path fill-rule="evenodd" d="M3 5.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3 8a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9A.5.5 0 0 1 3 8zm0 2.5a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5z"/>
				</svg>
				Status:
			</td>
			<td>
				<span class="badge badge-warning mb-2">
					{{$transaction->request_status->name}}
				</span>
					@include('transactions.partials.edit-status')
			</td>
		</tr>
		{{-- Status end --}}

		{{-- Date needed start --}}
		<tr>
			<td class="font-weight-bold">
				<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-calendar3-week" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
					<path fill-rule="evenodd" d="M14 0H2a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z"/>
					<path fill-rule="evenodd" d="M12 7a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-5 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm2-3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-5 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
				</svg>
				Date Needed:
			</td>
			<td>{{date("M d, Y", strtotime($transaction->date_needed)) }}</td>
		</tr>
		{{-- Date needed end --}}

		{{-- Date return start --}}
		<tr>
			<td class="font-weight-bold">
				<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-calendar3-week" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
					<path fill-rule="evenodd" d="M14 0H2a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z"/>
					<path fill-rule="evenodd" d="M12 7a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-5 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm2-3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-5 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
				</svg>
				Date Return:
			</td>
			<td>{{date("M d, Y", strtotime($transaction->date_return)) }}</td>
		</tr>
		{{-- Date return end --}}

	</table>
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Username:</th>
				<th>Unit Name:</th>
				<th>Unit Code:</th>
				<th>Unit Status:</th>
			</tr>
		</thead>
		<tbody>
			@foreach($transaction->products as $product)
			<tr>
				<td>{{ucfirst($transaction->user->name) }}</td>
				<td>{{$product->name}}</td>
				<td>{{$product->product_code}}</td>
				<td>{{$product->product_status->name}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
			