<div class="card">
  <div class="card-header" id="headingOne">
    <h2 class="mb-0">
      <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse{{$transaction->id}}" aria-expanded="true" aria-controls="collapseOne">
       {{$transaction->transaction_code}} 

       

       <span class="badge 
          badge-{{$transaction->request_status_id === 1? "warning" :
          ($transaction->request_status_id === 2 ? "primary" :
          ($transaction->request_status_id === 3 ? "danger" : "success"))

        }}">
         {{$transaction->request_status->name}}
       </span>
     </button>
   </h2>
  </div>
  <div id="collapse{{$transaction->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
       @include('transactions.partials.summary')
     </div>
  </div>
</div>