<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sports-E</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Heebo&display=swap" rel="stylesheet">

    <!-- Favicon -->
    <link rel="icon" href="/images/favicon1.png" type="image/gif" sizes="18x18">

    <!-- Styles -->
    {{-- <link rel="stylesheet" href="/css/style.css"> --}}
     <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>


    <div class="flex-center position-ref full-height">
        @if (Route::has('login'))
        <div class="top-right links">
            @auth
            <a href="{{ url('/home') }}">Home</a>
            @else
            <a href="{{ route('login') }}">Login</a>

            @if (Route::has('register'))
            <a href="{{ route('register') }}">Register</a>
            @endif
            @endauth
        </div>
        @endif

        <div class="content">
            <div class="title m-b-md">
                <h2><span>Sports-E.</span> Collections</h2>
            </div>
        </div>
    </div>
    <div class="container py-3" id="first-section">
        <div class="row">
            <div class="col-12 ">
                <h1 class="text-center font-weight-bold text-uppercase my-3" id="how">How it works?</h1>
                <div class="d-block d-md-flex justify-content-around align-items-center mx-auto mt-5" id="test">
                    <div class="text-center details mx-auto mx-md-0">
                        <img src="/images/click.png" alt="">
                        <h4 class="my-2 font-weight-bold text-uppercase">Choose</h4>
                        <p>Choose a unit that you want to rent. </p>
                    </div>
                    <div class="text-center details mx-auto mx-md-0">
                        <img src="/images/calendar.png" alt="">
                        <h4 class="my-2 font-weight-bold text-uppercase">Select</h4>
                        <p>Select your rent and return date of your unit request. </p>
                    </div>
                    <div class="text-center details mx-auto mx-md-0">
                        <img src="/images/submit.png" alt="">
                        <h4 class="my-2 font-weight-bold text-uppercase">Submit</h4>
                        <p>Submit your desired unit and check your summary request.</p>
                    </div>
                    <div class="text-center details mx-auto mx-md-0">
                        <img src="/images/activation.png" alt="">
                        <h4 class="my-2 font-weight-bold text-uppercase">Wait</h4>
                        <p>Wait for the confirmation of your request.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid my-5" id="featured-section">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center text-uppercase font-weight-bold" id="feat">Featured Units</h1>
            </div>
            <div class="col-12 ">
                <div class="d-md-flex justify-content-around mx-auto text-center my-0 my-md-2">
                    <img src="/images/featured1.jpg" class="my-1 my-md-0" alt="">
                    <img src="/images/featured2.jpg" class="my-1 my-md-0" alt="">
                    <img src="/images/featured3.jpg" class="my-1 my-md-0" alt="">
                    <img src="/images/featured4.jpg" class="my-1 my-md-0" alt="">
                </div>
            </div>
            <div class="col-12">
                <div class="d-md-flex justify-content-around mx-auto text-center my-0 my-md-2">
                    <img src="/images/featured6.jpg" class="my-1 my-md-0" alt="">
                    <img src="/images/featured5.jpg" class="my-1 my-md-0" alt="">
                    <img src="/images/featured7.jpg" class="my-1 my-md-0" alt="">
                </div>
            </div>
        </div>
    </div>

    {{-- <div class="container-fluid">
        <nav class="footer navbar navbar-expand navbar-light bg-dark shadow-lg rounded ">
            <div class="d-block mx-auto">
                <div class="collapse navbar-collapse">  
                    <h5 class="text-white">&copy; Sports-E. Collections 2020</h5>  
                </div>
            </div>
        </nav>
    </div> --}}
    <div class="text-center" id="footer">
        <h5 class="text-white text-uppercase">&copy; Sports-E. Collections 2020</h5>  
    </div>
    

    


</body>
</html>
